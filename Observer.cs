﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Windows.Controls;

namespace SWP_UE5
{
    class Observer : IObserver
    {
        public List<IMediator> ConcMediators { get; set; }

        public Observer()
        {
            ConcMediators = new List<IMediator>();
        }

        public void AddConcMediator(IMediator button_mediator)
        {
            ConcMediators.Add(button_mediator);
        }

        public void RemoveConcMediator(IMediator button_mediator)
        {
            ConcMediators.Remove(button_mediator);
        }

        public void Update()
        {
            foreach (IMediator conc_mediator in ConcMediators)
            {
                conc_mediator.Update();
            }
        }
    }
}
