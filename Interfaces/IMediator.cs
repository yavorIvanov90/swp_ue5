﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Windows.Controls;

namespace SWP_UE5
{
    public interface IMediator
    {
        List<Button> Buttons { get; set; }
        ListSubject ListSubject { get; set; }

        void AddExtendedButton(Button button);
        void SetListSubject(ListSubject listSubject);
        void Update();
    }
}
