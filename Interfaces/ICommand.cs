﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SWP_UE5.Interfaces
{
    public interface ICommand
    {
        void ChangeIsEnabled(bool value);
    }
}
