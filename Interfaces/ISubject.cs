﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SWP_UE5
{
    public interface ISubject
    {
        void Register(IObserver observer);
        void Unregister(IObserver observer);
        private void Notify() { }
    }
}
