﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Windows.Controls;
using System.Windows.Documents;

namespace SWP_UE5
{
    public interface IObserver
    {
        List<IMediator> ConcMediators { get; set; }

        void AddConcMediator(IMediator conc_mediator);
        void RemoveConcMediator(IMediator conc_mediator);
        void Update();
    }
}
