﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SWP_UE5
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private IObserver ListObserver { get; set; }
        private ListSubject ListSubject { get; set; }

        public MainWindow()
        {
            InitializeComponent();
            Title = "SWP UE5";

            button_edit.IsEnabled = false;
            button_delete.IsEnabled = false;
            button_feature_3.IsEnabled = true;

            for (int i = 1; i <= 5; i++)
            {
                ListBoxItem item = new ListBoxItem
                {
                    Content = $"Item{i}"
                };
                list.Items.Add(item);
            }

            ListSubject = new ListSubject(list);
            ListObserver = new Observer();
            ListSubject.Register(ListObserver);


            //Create Concrete Mediator to notify(change) the Buttons
            ConcreteMediator conc_mediator = new ConcreteMediator();


            conc_mediator.AddExtendedButton(button_edit);
            conc_mediator.AddExtendedButton(button_delete);
            conc_mediator.AddExtendedButton(button_feature_3);
            conc_mediator.SetListSubject(ListSubject);

            ListObserver.AddConcMediator(conc_mediator);
        }

        private void Selected(object sender, RoutedEventArgs e)
        {
            if (e.Source is ListBox)
            {
                ListSubject.setCountSelectedItems();
            }
        }
    }
}
