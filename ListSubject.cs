﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Controls;

namespace SWP_UE5
{
    public class ListSubject : ISubject
    {
        private ListBox ListBox { get; set; }
        private List<IObserver> Observers { get; set; }
        private int selectedItems = 0;
        public ListSubject(ListBox listBox)
        {
            Observers = new List<IObserver>();
            ListBox = listBox;
        }

        private void Notify()
        {
            foreach(IObserver observer in Observers)
            {
                observer.Update();
            }
        }

        public void Register(IObserver observer)
        {
            Observers.Add(observer);
        }

        public void Unregister(IObserver observer)
        {
            Observers.Remove(observer);
        }

        public int GetCountSelectedItems()
        {
            return selectedItems;
        }

        public void setCountSelectedItems()
        {
            selectedItems = ListBox.SelectedItems.Count;
            Notify();
        }
    }
}
