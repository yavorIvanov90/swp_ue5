﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Windows.Controls;

namespace SWP_UE5
{
    public class ConcreteMediator : IMediator
    {
        public List<Button> Buttons { get; set; }
        public ListSubject ListSubject { get; set; }

        public ConcreteMediator()
        {
            Buttons = new List<Button>();
        }
        public void AddExtendedButton(Button button)
        {
            Buttons.Add(button);
        }

        public void SetListSubject(ListSubject listSubject)
        {
            ListSubject = listSubject;
        }

        public void Update()
        {
            int items = ListSubject.GetCountSelectedItems();
            if (items == 1)
            {
                Buttons.Find(s => s.Name == "button_edit").IsEnabled = true;
                Buttons.Find(s => s.Name == "button_delete").IsEnabled = true;
                Buttons.Find(s => s.Name == "button_feature_3").IsEnabled = false;
            }
            if (items > 1)
            {
                Buttons.Find(s => s.Name == "button_edit").IsEnabled = false;
                Buttons.Find(s => s.Name == "button_delete").IsEnabled = true;
                Buttons.Find(s => s.Name == "button_feature_3").IsEnabled = false;
            }
            if (items == 0)
            {
                Buttons.Find(s => s.Name == "button_edit").IsEnabled = false;
                Buttons.Find(s => s.Name == "button_delete").IsEnabled = false;
                Buttons.Find(s => s.Name == "button_feature_3").IsEnabled = true;
            }
        }
    }
}
